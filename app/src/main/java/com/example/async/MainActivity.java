package com.example.async;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

public class MainActivity extends Activity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                try {
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Toast.makeText(this, "Конец паузы", Toast.LENGTH_SHORT).show();
                break;
            case R.id.button2:
                new MyTask().execute();
                break;
        }
    }

    class MyTask extends AsyncTask {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            ((TextView)findViewById(R.id.textView)).setText("Begin");
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            ((TextView)findViewById(R.id.textView)).setText("End");
            Toast.makeText(getApplicationContext(), "Конец асинхронной паузы", Toast.LENGTH_SHORT).show();
        }
    }
}
